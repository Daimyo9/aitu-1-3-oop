package kz.aitu.oop.examples.assignment3;

import java.util.Arrays;

public class MyString {

    private int[] values;

    // keep the array values internally
    public MyString(int[] values) {
        this.values = values;
    }

   // return the number of values that are stored
    public int length() {
        return values.length;
    }

    // return the value stored at position or -1 if position is not available
    public int valueAt(int position) {
        if(position < 0 || position >= this.length()) return -1;
        return values[position];
    }

    // return true if value is stored, otherwise false
    public boolean contains(int value) {
        for (int i = 0; i < values.length; i++) {
            if(values[i] == value) return true;
        }
        return false;
    }

    // count for how many time value is stored
    public int count(int value) {
        int count = 0;
        for (int i = 0; i < values.length; i++) {
            if(values[i] == value) count++;
        }
        return count;
    }

    //print the stored values ... }
    public void print() {
        for (int i = 0; i < values.length; i++) {
            System.out.println(values[i]);
        }
    }

    public boolean equals(MyString anotherString) {
        if(this.length() != anotherString.length()) return false;

        for (int i = 0; i < values.length; i++) {
            if(values[i] != anotherString.valueAt(i)) return false;
        }
        return true;
    }
}
